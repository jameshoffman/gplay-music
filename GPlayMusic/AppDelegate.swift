//
//  AppDelegate.swift
//  GPlayMusic
//
//  Created by James Hoffman on 2016-06-24.
//  Copyright © 2016 jhoffman.ca. All rights reserved.
//

import Cocoa
import MediaKeyTap

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, MediaKeyTapDelegate {
    
    //
    //MARK: Constants
    //
    
    static let PLAY_PAUSE_NOTIFICATION_KEY = "ca.jhoffman.gplaymusic.play.pause.notification.key"
    static let PREVIOUS_NOTIFICATION_KEY = "ca.jhoffman.gplaymusic.previous.notification.key"
    static let NEXT_NOTIFICATION_KEY = "ca.jhoffman.gplaymusic.next.notification.key"
    
    //
    //MARK: Members
    //
    
    private(set) var mediaKeyTap: MediaKeyTap?
    
    //
    //MARK: NSApplicationDelegate
    //
    
    func applicationDidFinishLaunching(notification: NSNotification) {
        mediaKeyTap = MediaKeyTap(delegate: self)
        mediaKeyTap?.start()
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication) -> Bool {
        return true
    }
    
    //
    //MARK: MediaKeyTapDelegate
    //
    
    func handleMediaKey(mediaKey: MediaKey, event: KeyEvent) {
        let notificationKeys: [MediaKey: String] = [.PlayPause: AppDelegate.PLAY_PAUSE_NOTIFICATION_KEY,
                                                    .Previous: AppDelegate.PREVIOUS_NOTIFICATION_KEY,
                                                    .Next: AppDelegate.NEXT_NOTIFICATION_KEY,
                                                    .Rewind: AppDelegate.PREVIOUS_NOTIFICATION_KEY,
                                                    .FastForward: AppDelegate.NEXT_NOTIFICATION_KEY]

        if let notificationKey = notificationKeys[mediaKey] {
            NSNotificationCenter.defaultCenter().postNotificationName(notificationKey, object: nil)
        }
    }
}

