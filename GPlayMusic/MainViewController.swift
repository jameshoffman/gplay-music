//
//  ViewController.swift
//  GPlayMusic
//
//  Created by James Hoffman on 2016-06-24.
//  Copyright © 2016 jhoffman.ca. All rights reserved.
//

import Cocoa
import WebKit

class MainViewController: NSViewController {

    //
    //MARK: IBOutlets
    //
    
    @IBOutlet weak var webView: WebView!
    
    //
    //MARK: Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.mainFrame.loadRequest(NSURLRequest(URL: NSURL(string: "https://play.google.com/music")!))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.playPausePressed), name: AppDelegate.PLAY_PAUSE_NOTIFICATION_KEY, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.nextPressed), name: AppDelegate.NEXT_NOTIFICATION_KEY, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.previousPressed), name: AppDelegate.PREVIOUS_NOTIFICATION_KEY, object: nil)
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
        let window = self.view.window!
        
        window.setContentSize(NSSize(width: 965, height: 870))
        window.contentMinSize = NSSize(width: 965, height: 156)
        window.title = "GPlay Music"
    }
    
    //
    //MARK: MainViewController
    //
    
    private func pressButton(buttonId: String) {
        let clickJs = "document.getElementById('\(buttonId)').click()"
        
        self.webView?.stringByEvaluatingJavaScriptFromString(clickJs)
    }
    
    func playPausePressed() {
        pressButton("player-bar-play-pause")
    }
    
    func nextPressed() {
        pressButton("player-bar-forward")
    }
    
    func previousPressed() {
        pressButton("player-bar-rewind")
    }
}

